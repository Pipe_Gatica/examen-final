#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import json


def load_file():

    with open("iris.json") as file:
        cargar = json.load(file)

    return cargar


def mostrar_especies(datos):

    lista =[]
    for i in datos:
        lista.append(i['species'])

    # Vamos a contar todas las veces que se repita cada elemento
    a = lista.count('setosa')
    b = lista.count('versicolor')
    c = lista.count('virginica')

    # Se eliminaran todos los elementos repetidos hasta que el elemento
    # llegue a 1, osea que ya no esta repetido
    while a > 1:
        lista.remove('setosa')
        a = lista.count('setosa')

    while b > 1:
        lista.remove('versicolor')
        b = lista.count('versicolor')

    while c > 1:
        lista.remove('virginica')
        c = lista.count('virginica')

    print('\nPrimera especie: ', lista[0])
    print('Segunda especie: ', lista[1])
    print('Tercera especie: ', lista[2])


def calcular_promedio(datos):

    lista1 = []
    lista2 = []
    leer = (linea for i,linea in enumerate(datos) if i>=0 and i<50)
    for linea in leer: # lee las lineas correspondientes a la primera especie
        # En una lista se agregaran los datos del largo de los petalos
        # y en otra lista se agregaran los datos del ancho de los petalos
        lista1.append(linea['petalLength'])
        lista2.append(linea['petalWidth'])

    # Sumamos todos los elementos y los dividimos en 50 para obtener el
    # promedio del largo y ancho de los petalos de la especie
    alto_setosa = sum(lista1) / 50
    ancho_setosa = sum(lista2) / 50

    print('\nSetosa: Promedio de altura de petalos ---->', alto_setosa)
    print('        Promedio de anchura de petalos --->', ancho_setosa)

    # Repetimos el procedimiento para la segunda especie
    lista3 = []
    lista4 = []
    leer = (linea for i,linea in enumerate(datos) if i>=50 and i<100)
    for linea in leer: # lee las lineas correspondientes a la segunda especie
        lista3.append(linea['petalLength'])
        lista4.append(linea['petalWidth'])

    alto_versicolor = sum(lista3) / 50
    ancho_versicolor = sum(lista4) / 50

    print('Versicolor: Promedio de altura de petalos ---->', alto_versicolor)
    print('            Promedio de anchura de petalos --->', ancho_versicolor)

    # Repetimos el procedimiento para la tercera especie
    lista5 = []
    lista6 = []
    leer = (linea for i,linea in enumerate(datos) if i>=100 and i<150)
    for linea in leer: # lee las lineas correspondientes a la tercera especie
        lista5.append(linea['petalLength'])
        lista6.append(linea['petalWidth'])

    alto_virginica = sum(lista5) / 50
    ancho_virginica = sum(lista6) / 50

    print('Virginica: Promedio de altura de petalos ---->', alto_virginica)
    print('           Promedio de anchura de petalos --->', ancho_virginica)

    print('\n')
    # Comparamos que especie tiene el promedio de altura y anchura mayor
    # Setosa
    if alto_setosa > alto_versicolor and alto_setosa > alto_virginica:
        l = 'Promedio de petalos mas altos ----> {}, de la especie Setosa'
        print(l.format(alto_setosa))

    if ancho_setosa > ancho_versicolor and ancho_setosa > ancho_virginica:
        l = 'Promedio de petalos mas anchos ---> {}, de la especie Setosa'
        print(l.format(ancho_setosa))

    # Versicolor
    if alto_versicolor > alto_setosa and alto_versicolor > alto_virginica:
        l = 'Promedio de petalos mas altos ----> {}, de la especie Versicolor'
        print(l.format(alto_versicolor))

    if ancho_versicolor > ancho_setosa and ancho_versicolor > ancho_virginica:
        l = 'Promedio de petalos mas anchos ---> {}, de la especie Versicolor'
        print(l.format(ancho_versicolor))

    # Virginica
    if alto_virginica > alto_setosa and alto_virginica > alto_versicolor:
        l = 'Promedio de petalos mas altos ----> {}, de la especie Virginica'
        print(l.format(alto_virginica))

    if ancho_virginica > ancho_setosa and ancho_virginica > ancho_versicolor:
        l = 'Promedio de petalos mas anchos ---> {}, de la especie Virginica'
        print(l.format(ancho_virginica))


def archivo_json(datos):

    # El error en esta funcion es que despues de guardar el archivo archivo json
    # cuando el bucle del for se va a ejecutar nuevamente pero en un x+1,
    # osea que la siguiente linea, no cargamos la informacion que tiene el
    # archivo que fue ingresado en el bucle anterior, despues de haber creado
    # el archivo. habia que cargarlo, ingresar informacion y guardar
    # y eso fue lo que me falto
    guardar_setona(datos)
    guardar_versicolor(datos)
    guardar_virginica(datos)
    print('\n¡Archivos creados exitosamente!')


def guardar_setona(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=1 and i<51)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("setosa.json", 'w') as file:
            json.dump(x, file)


def guardar_versicolor(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=51 and i<101)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("versicolor.json", 'w') as file:
            json.dump(x, file)

def guardar_virginica(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=101 and i<151)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("virginica.json", 'w') as file:
            json.dump(x, file)


def mas_alto(datos):

    lista1 = []
    leer = (linea for i,linea in enumerate(datos) if i>=0 and i<50)
    for linea in leer: # lee las lineas correspondientes a la primera especie
        # Agrega el largo del sepal de cada muestra de la primera especie
        # a una lista
        lista1.append(linea['sepalLength'])
    # Ordenamos la lista de menor a mayor y tomamos el ultimo dato que seria el
    # mayor de la especie
    lista1.sort()
    largo_setosa = lista1[49]

    # Repetimos el procedimiento para la segunda especie
    lista2 = []
    leer = (linea for i,linea in enumerate(datos) if i>=50 and i<100)
    for linea in leer:
        lista2.append(linea['sepalLength'])
    lista2.sort()
    largo_versicolor = lista2[49]

    # Repetimos el procedimiento para la tercera especie
    lista3 = []
    leer = (linea for i,linea in enumerate(datos) if i>=100 and i<150)
    for linea in leer:
        lista3.append(linea['sepalLength'])
    lista3.sort()
    largo_virginica = lista3[49]

    # Comparamos el largo de cada especie y se muestra la medida y especie
    # del sepalo mas largo
    if largo_setosa > largo_versicolor and largo_setosa > largo_virginica:
        l = '\nEl sepalo mas grande mide {} y es de la especie Setosa'
        print(l.format(largo_setosa))

    if largo_versicolor > largo_setosa and largo_versicolor > largo_virginica:
        l = '\nEl sepalo mas grande mide {} y es de la especie Versicolor'
        print(l.format(largo_versicolor))

    if largo_virginica > largo_setosa and largo_virginica > largo_versicolor:
        l = '\nEl sepalo mas grande mide {} y es de la especie Virginica'
        print(l.format(largo_virginica))


def main():

    datos = load_file()

    while True:
        print('\nPresione')
        print('1 mostrar las especies')
        print('2 calcular el promedio de alto y ancho de los petalos')
        print('3 mostrar los registros que se encuentran en el rango promedio')
        print('4 buscar la medida y especie del sepalo mas alto')
        print('5 separar en archivos json cada especie')
        print('6 Cerrar programa')
        opt = int(input('Elija una opcion: '))

        if opt == 1: # Listo
            mostrar_especies(datos)
            pass

        elif opt == 2: # Listo
            calcular_promedio(datos)
            pass

        elif opt == 3: # Falta
            pass

        elif opt == 4: # Listo
            mas_alto(datos)

        elif opt == 5: # A medias
            archivo_json(datos)
            pass

        elif opt == 6:
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass


if __name__ == '__main__':
    main()
